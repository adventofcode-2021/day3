const fs = require("fs");

const dataLines = fs
  .readFileSync("input-part1.txt")
  .toString()
  .trim()
  .split("\n");
const lineCount = dataLines.length;
const lineLength = dataLines[0].length;
const gammaRate = dataLines
  .map(convertToBitArray)
  .reduce(countSetBits)
  .map((x) => findMostSetBit(x, lineCount))
  .reduce(bitArrayToNumber);

const epsilonRate = gammaToEpsilon(gammaRate, lineLength);

console.log("gammaRate", gammaRate.toString(2));
console.log("epsilonRate", epsilonRate.toString(2));
console.log(gammaRate * epsilonRate);

function gammaToEpsilon(gammaRate, wordSize) {
  const epsilonRateMask = Number.MAX_SAFE_INTEGER >>> (32 - wordSize);
  return ~gammaRate & epsilonRateMask;
}

function convertToBitArray(dataLine) {
  return Array.from(dataLine).map((x) => parseInt(x));
}
function countSetBits(previous, current) {
  return previous.map((value, index) => value + current[index]);
}
function findMostSetBit(setBitCount, maxBitCount) {
  return Math.trunc(setBitCount / (maxBitCount / 2));
}
function bitArrayToNumber(previous, current) {
  return (previous << 1) + current;
}
