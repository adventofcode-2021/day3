const fs = require("fs");

const data = fs
  .readFileSync("input-part2.txt")
  .toString()
  .trim()
  .split("\n");

const filterSetBits = (x, y) => x == y;
const filterUnsetBits = (x, y) => x != y;

const oxygenRating = findRating(data, filterSetBits);
const co2Rating = findRating(data, filterUnsetBits);

console.log(oxygenRating * co2Rating);

function findRating(values, bitFilter) {
  let bitArray = values.map(convertToBitArray);

  for (let i = 0; bitArray.length != 1; i++) {
    const mostSetBits = bitArray
      .reduce(countSetBits)
      .map((x) => findMostSetBit(x, bitArray.length));

    bitArray = bitArray.filter((x) => bitFilter(x[i], mostSetBits[i]));
  }
  return bitArray[0].reduce(bitArrayToNumber);
}

function convertToBitArray(dataLine) {
  return Array.from(dataLine).map((x) => parseInt(x));
}
function countSetBits(previous, current) {
  return previous.map((value, index) => value + current[index]);
}
function findMostSetBit(setBitCount, maxBitCount) {
  return Math.trunc(setBitCount / (maxBitCount / 2));
}
function bitArrayToNumber(previous, current) {
  return (previous << 1) + current;
}
